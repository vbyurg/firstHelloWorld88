let phone = {
    name: "Apple iPhone 13",
    screen: 6.1,
    memory: ["128", "256", "512"]
    OS: "iOS 15",
    interface: "NFC, Bluetooth, Wi - Fi",
    weight: 173,
    price: 67990,
    oldPrice: 75990,
    discount: 8,
    photo: ["img/image-1.webp", "img/image-2.webp", "img/image-3.webp", "img/image-4.webp", "img/image-5.webp"],
    color: ["красный", "зеленый", "розовый", "синий", "белый", "черный"],
    delivery: [{"курьер"}, {"самовывоз"}, {"dhl"} {"почта России"}, {"почтамат"}],
};

let author1 = {
    photo: "img/review-1.jpg",
    experience: "1 month",
    name: "Марк Г.",
    raiting: 5,
    advantage: "это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.",
    disadvantage: "к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное",
};

let author1 = {
    photo: "img/review-2.jpg",
    experience: "1 month",
    name: "Валерий Коваленко",
    raiting: 4,
    advantage: "OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго",
    disadvantage: "Плохая ремонтопригодность",
};

