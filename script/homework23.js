"use strict"

// Упражнение 1

let count = +prompt("Введите число");

let interval = setInterval(() => {

    if (isNaN(count) || count == '') {
        clearInterval(interval);
        console.log("Вы ввели не число")
    } else {
        count = parseInt(count) - 1;
        console.log("Осталось: " + count);
    }

    if (count === 0) {
        clearInterval(interval);
        console.log("Время вышло!")
    }

}, 1000)

// Упражнение 2

let link = fetch("https://reqres.in/api/users")

link.then(function (response) {
    return response.json()
}).then(function (response) {
    const users = response.data;
    console.log(users)
    console.log(`Получили пользователей: ${users.length}`)
    users.forEach(function (user) { // Количество пользователей user.lenght
        // console.log(user)
        console.log(` - ${user.first_name} ${user.last_name} (${user.email})`)
    })
})
