// Упражнение 1

let a = '$100'
let b = '300$'

let summ = +(a.slice(1)) + (parseInt(b))

console.log(summ);

// Упражнение 2

let message = ' привет, медвед      ';
message = message.trim().substring(0, 1).toUpperCase() + message.substring(2);

console.log(message);


// Упражнение 3

let age = +prompt('Сколько вам лет?');

if (age >= 0 && age <= 3) {
    alert(`Вам ${age} лет и вы младенец`)
} else if (age >= 4 && age <= 11) {
    alert(`Вам ${age} лет и вы ребенок`)
} else if (age >= 12 && age <= 18) {
    alert(`Вам ${age} лет и вы подросток`)
} else if (age >= 19 && age <= 40) {
    alert(`Вам ${age} лет и вы познаёте жизнь`)
} else if (age >= 41 && age <= 80) {
    alert(`Вам ${age} лет и вы познали жизнь`)
} else if (age >= 81) {
    alert(`Вам ${age} лет и вы долгожитель`)
}

// Упражнение 4
let message = 'Я работаю со строками как профессионал!';

let count = message.split(' ').length

console.log(count);
