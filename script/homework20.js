// Упражнение 1

for (let i = 2; i <= 20; i++) {
    if (i % 2 == 0) {
        console.log(i);
    }
};

// Упражнение 2

let sum = 0;

for (i = 1; i <= 3; i++) {
    let i = +prompt("Введите");

    if (!i) break;

    sum += i;
}

if (sum) {
    alert("Сумма: " + sum)
} else {
    alert("Ошибка, вы ввели не число");
}

// Упражнение 3

function getNameOfMonth(month) {

    switch (month) {
        case 0:
            alert('Январь');
            break;
        case 1:
            alert('Февраль');
            break;
        case 2:
            alert('Март');
            break;
        case 3:
            alert('Апрель');
            break;
        case 4:
            alert('Май');
            break;
        case 5:
            alert('Июнь');
            break;
        case 6:
            alert('Июль');
            break;
        case 7:
            alert('Август');
            break;
        case 8:
            alert('Сентябрь');
            break;
        case 9:
            alert('Октябрь');
            break;
        case 10:
            alert('Ноябрь');
            break;
        case 11:
            alert('Декабрь');
            break;
    };
}

getNameOfMonth(2);

// ====================

let month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

for (i = 0; i <= 11; i++) {
    if (month[i] == "Октябрь") continue;
    console.log(month[i]);
};
