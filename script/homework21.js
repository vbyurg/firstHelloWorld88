"user strict"

// Упражнение 1

/**
 * Проверяет наличие / отсутствие свойств в объекте 
 * @param {} obj Ссвойства в объекте
 * @returns true - объект пустой, false - в объекте есть свойства
 */

let obj = {};

function isEmpty() {
    for (let key in obj) {
        return false;
    }
    return true;
}



alert(isEmpty(obj));


// Упражненеие 2
// см. data.js

// Упражнение 3



let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};


/**
 * Суммирует все значения в объекте, изменив их на указанный процент
 * @param {number} percent - процент, на который изменится сумма всех значений объекта
 */
function raiseSalary(percent) {
    let sum = 0;

    for (let key in salaries) {
        sum += salaries[key]
    };

    let perc = Math.floor((sum * percent) / 100)

    console.log(perc);
}

raiseSalary();