"use strict"

// Упражнение 1

const array = [];

const getSumm = () => {
    let sum = 0;
    let arr = array.flat(Infinity);

    arr.forEach((elem) => {
        if (typeof elem !== "number") {
            return;
        } else {
            sum += elem
        }
    })
    console.log(sum);
}

getSumm(array);


// Упражнение 2
// см. data.js

// Упражнение 3

let cart = [45454];

// Добавление товара
const addToCart = (product) => {
    let item = cart.includes(product);

    if (item) return;

    cart.push(product)
}
addToCart()

// Удаление товара
const removeFromCart = (product) => {
    cart = cart.filter(function (produciID) {
        return product !== produciID;
    })
}

removeFromCart()

